#!/usr/bin/env bash

## configuration de wiki.org 
# configuration du serveur dns

# La commande ci-dessous crée un répertoire /etc/named sur la machine dwikiorg
himage drtiutre mkdir -p /etc/named

# La commande ci-dessous copie le fichier se trouvant dans wiki.org/named.conf sur la machine dwikiorg dans le répertoire /etc/
hcp rt.iut.re/named.conf drtiutre:/etc/.

# La commande ci-dessous copie tous les fichiers se trouvant dans wiki.org/* dans le répertoire  /etc/named sur la machine dwikiorg
hcp rt.iut.re/* drtiutre:/etc/named/.

# la commande ci-dessou supprime (commande rm) le fichier /etc/named/named.conf se trouvant sur la machine dwikiorg
himage drtiutre rm /etc/named/named.conf 


## configuration de iut.re 
# configuration du serveur dns
# Les fichiers ci-dessous ne sont pas présents
# du coup je commentes

#himage diutre mkdir -p /etc/named
#hcp iut.re/named.conf diutre:/etc/.
#hcp iut.re/* diutre:/etc/named/.
#himage diutre rm /etc/named/named.conf 


## configuration de pc1
# resolv.conf
# Les fichiers ci-dessous ne sont pas présents
# du coup je commentes

#hcp pc1/resolv.conf pc1:/etc/.
